#Install script to set up dune and dumux for the pub-module bringedal2021a

#The following git repositories will be set up with the listed branch and commit:
#dune-common               releases/2.7    20a8172d68a26d3f87457c3e2f2a7a7e24f609db
#dune-geometry             releases/2.7    fb75493b000410cbd157927f80c68ec9e86c5553
#dune-grid                 releases/2.7    ecadcb33e06dac641967131aacd36968fe1bb45b
#dune-localfunctions       releases/2.7    ad588f9599b12b1615e760ae0106594f2059ab3e
#dune-istl                 releases/2.7    399ed5996e0b15c2038290810ad0f07d5f1e4c30
#dune-foamgrid             releases/2.7    d49187be4940227c945ced02f8457ccc9d47536a
#dune-alugrid              releases/2.7    7b55e36305bded9dd0d14e5f7c9b40f28f228aed
#dumux                     releases/3.2    1ab3ac40aab7fa14ec89f23291ed1ce3b2a4183d
#bringedal2021a            main

# defines a function to exit with error message
exitWith ()
{
    echo "\n$1"
    exit 1
}

# dune-common
# releases/2.7 # 20a8172d68a26d3f87457c3e2f2a7a7e24f609db # 2020-05-18 10:07:11 +0000 # Dominic Kempf
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.7
if ! git reset --hard 20a8172d68a26d3f87457c3e2f2a7a7e24f609db; then exitWith "--Error: could not setup dune-common"; fi
echo "-- Successfully set up the module dune-common --"
cd ..

# dune-geometry
# releases/2.7 # fb75493b000410cbd157927f80c68ec9e86c5553 # 2020-04-29 04:38:04 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.7
if ! git reset --hard fb75493b000410cbd157927f80c68ec9e86c5553; then exitWith "--Error: could not setup dune-geometry"; fi
echo "-- Successfully set up the module dune-geometry --"
cd ..

# dune-grid
# releases/2.7 # ecadcb33e06dac641967131aacd36968fe1bb45b # 2020-07-02 09:50:01 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.7
if ! git reset --hard ecadcb33e06dac641967131aacd36968fe1bb45b; then exitWith "--Error: could not setup dune-grid"; fi
echo "-- Successfully set up the module dune-grid --"
cd ..

# dune-localfunctions
# releases/2.7 # ad588f9599b12b1615e760ae0106594f2059ab3e # 2020-05-15 07:48:41 +0000 # Dominic Kempf
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.7
if ! git reset --hard ad588f9599b12b1615e760ae0106594f2059ab3e; then exitWith "--Error: could not setup dune-localfunctions"; fi
echo "-- Successfully set up the module dune-localfunctions --"
cd ..

# dune-istl
# releases/2.7 # 399ed5996e0b15c2038290810ad0f07d5f1e4c30 # 2020-05-14 20:58:36 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.7
if ! git reset --hard 399ed5996e0b15c2038290810ad0f07d5f1e4c30; then exitWith "--Error: could not setup dune-istl"; fi
echo "-- Successfully set up the module dune-istl --"
cd ..

# dune-foamgrid
# releases/2.7 # d49187be4940227c945ced02f8457ccc9d47536a # 2020-01-06 15:36:03 +0000 # Timo Koch
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout releases/2.7
if ! git reset --hard d49187be4940227c945ced02f8457ccc9d47536a; then exitWith "--Error: could not setup dune-foamgrid"; fi
echo "-- Successfully set up the module dune-foamgrid --"
cd ..

# dune-alugrid
# releases/2.7 # 7b55e36305bded9dd0d14e5f7c9b40f28f228aed # 2020-04-20 11:59:41 +0200 # Robert K
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
cd dune-alugrid
git checkout releases/2.7
if ! git reset --hard 7b55e36305bded9dd0d14e5f7c9b40f28f228aed; then exitWith "--Error: could not setup dune-alugrid"; fi
echo "-- Successfully set up the module dune-alugrid --"
cd ..

# dumux
# releases/3.2 # 1ab3ac40aab7fa14ec89f23291ed1ce3b2a4183d # 2020-06-01 12:55:53 +0200 # Timo Koch
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout releases/3.2
if ! git reset --hard 1ab3ac40aab7fa14ec89f23291ed1ce3b2a4183d; then exitWith "--Error: could not setup dumux"; fi
echo "-- Successfully set up the module dumux --"
cd ..

# bringedal2021a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a.git
cd bringedal2021a
if ! git checkout main; then exitWith "--Error: could not setup bringedal2021a"; fi
echo "-- Successfully set up the module bringedal2021a --"
cd ..

echo "-- All modules haven been cloned successfully. Configuring project..."
if ! ./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all; then exitWith "--Error: could not configure project"; fi
echo "-- Configuring successful."
