Using dumux module bringedal2021a with docker
=============================================

In order to run simulations of this module look
at the convenience script [script docker_bringedal2021a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a/-/blob/main/docker/docker_bringedal2021a.sh).

The simplest way is to spin up a container
is creating a new folder `DUMUX`
```bash
mkdir DUMUX
```
change to the new folder
```bash
cd DUMUX
```
Download the [script docker_bringedal2021a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a/-/blob/main/docker/docker_bringedal2021a.sh) from the git repository and open the module image by running
```bash
chmod u+x docker_bringedal2021a.sh
./docker_bringedal2021a.sh open
```

The container will spin up. It will mount the `DUMUX`
directory into the container at `/dumux/shared`. Put files
in this folder to share them with the host machine. This
could be e.g. VTK files produced by the simulation that
you want to visualize on the host machine.
