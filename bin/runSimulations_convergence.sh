#Run all simulations for the convergence study presented in the appendix of the paper additional to the ones performed in runSimulations_permeabilities.sh

cd build-cmake/appl/
echo "-- Compile executable..."
make instability_1pncmin_tpfa

echo "-------------------------------------------------------------------------------------------------------"
echo "--------------** Run simulations with different spatial discretization in x-direction **---------------"
echo "-------------------------------------------------------------------------------------------------------"
echo "- Simulation with K=1e-10 m^2, with dx = 0.003333 m corresponding to 3 cells per initial perturbation -"
./instability_1pncmin_tpfa params.input -Problem.Name instability_x_3cells -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20_3cells.dgf -Problem.InitialWaveLength 0.01 -TimeLoop.TEnd 2e5

echo "-------------------------------------------------------------------------------------------------------"
echo "-- Simulation with K=1e-10 m^2, with dx = 0.002 m corresponding to 5 cells per initial perturbation ---"
./instability_1pncmin_tpfa params.input -Problem.Name instability_x_5cells -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20_5cells.dgf -Problem.InitialWaveLength 0.01 -TimeLoop.TEnd 2e5

echo "-------------------------------------------------------------------------------------------------------"
echo "- Simulation with K=1e-10 m^2, with dx = 0.0005 m corresponding to 20 cells per initial perturbation -"
./instability_1pncmin_tpfa params.input -Problem.Name instability_x_20cells -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20_20cells.dgf -Problem.InitialWaveLength 0.01 -TimeLoop.TEnd 2e5

echo "-------------------------------------------------------------------------------------------------------"
echo "--------------** Run simulations with different spatial discretization in y-direction **---------------"
echo "-------------------------------------------------------------------------------------------------------"
echo "-- Simulation with K=1e-10 m^2, dy = 0.000984 m at the top and grading with 30 cells in y-direction ---"
./instability_1pncmin_tpfa params.input -Problem.Name instability_y_30cells -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20_y_30cells.dgf -Problem.InitialWaveLength 0.01 -TimeLoop.TEnd 2e5

echo "-------------------------------------------------------------------------------------------------------"
echo "-- Simulation with K=1e-10 m^2, dy = 0.000571 m at the top and grading with 35 cells in y-direction ---"
./instability_1pncmin_tpfa params.input -Problem.Name instability_y_35cells -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20_y_35cells.dgf -Problem.InitialWaveLength 0.01 -TimeLoop.TEnd 2e5

echo "-------------------------------------------------------------------------------------------------------"
echo "-- Simulation with K=1e-10 m^2, dy = 0.000115 m at the top and grading with 50 cells in y-direction ---"
./instability_1pncmin_tpfa params.input -Problem.Name instability_y_50cells -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20_y_50cells.dgf -Problem.InitialWaveLength 0.01 -TimeLoop.TEnd 2e5

echo "-------------------------------------------------------------------------------------------------------"
echo "- Simulation with K=1e-10 m^2, dy = 0.001 m at the top and uniform grid with 200 cells in y-direction -"
./instability_1pncmin_tpfa params.input -Problem.Name instability_y_200cells -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20_y_200cells.dgf -Problem.InitialWaveLength 0.01 -TimeLoop.TEnd 2e5

echo "-------------------------------------------------------------------------------------------------------"
echo "--------------** Run simulations with different temporal discretization **-----------------------------"
echo "-------------------------------------------------------------------------------------------------------"
echo "-- Simulation with K=1e-10 m^2, and a maximal time step size of dt=25 s -------------------------------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_dt25s -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.01 -TimeLoop.TEnd 2e5 -TimeLoop.DtInitial 25 -TimeLoop.MaxTimeStepSize 25

echo "-------------------------------------------------------------------------------------------------------"
echo "-- Simulation with K=1e-10 m^2, and a maximal time step size of dt=100 s ------------------------------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_dt100s -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.01 -TimeLoop.TEnd 2e5 -TimeLoop.DtInitial 100 -TimeLoop.MaxTimeStepSize 100

echo "-------------------------------------------------------------------------------------------------------"
echo "-- Simulation with K=1e-10 m^2, and a maximal time step size of dt=150 s ------------------------------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_dt150s -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.01 -TimeLoop.TEnd 2e5 -TimeLoop.DtInitial 150 -TimeLoop.MaxTimeStepSize 150

echo "----------------  All simulations performed. ----------------------------------------------------------"
