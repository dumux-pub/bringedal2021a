#Run all simulations for the investigation of different initial perturbations used in the paper additional to the ones performed in runSimulations_permeabilities.sh

cd build-cmake/appl/
echo "-- Compile executable..."
make instability_1pncmin_tpfa

echo "-------------------------------------------------------------------------------------------------------"
echo "-----------** Run simulations with different initial perturbations **----------------------------------"
echo "-------------------------------------------------------------------------------------------------------"
echo "------------- Simulation with K=1e-11 m^2, periodic inital perturbations at the top -------------------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-11_periodic_top -SpatialParams.referencePermeability 1e-11 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain false -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.03 -TimeLoop.TEnd 1e6

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-11 m^2, no initial perturbations -------------------------------------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-11_noPerturbations -SpatialParams.referencePermeability 1e-11 -Problem.PeriodicPertubations_xNaCl false -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.03 -TimeLoop.TEnd 1e6

echo "-------------------------------------------------------------------------------------------------------"
echo "- Simulation with K=1e-11 m^2, periodic initial perturbations in the whole domain with amplitude A=1e-12 -"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-11_periodic_A1e-12 -Problem.Amplitude 1e-12 -SpatialParams.referencePermeability 1e-11 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.03 -TimeLoop.TEnd 1e6

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-11 m^2, periodic initial perturbations in the whole domain with width of 50 cm which is not a multiple of the wavelength -"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-11_periodic_w50 -SpatialParams.referencePermeability 1e-11 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Problem.PertubeWholeDomain true -Grid.File grids/grid_w50_h20.dgf -Problem.InitialWaveLength 0.03 -TimeLoop.TEnd 1e6

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-11 m^2, random inital perturbations at the top -----------------------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-11_random_top -SpatialParams.referencePermeability 1e-11 -Problem.PeriodicPertubations_xNaCl false -Problem.RandomPertubations_xNaCl true -Problem.PertubeWholeDomain false -Problem.StddevInitialSaltMolFrac 1e-6 -Grid.File grids/grid_w60_h20_nonPeriodic.dgf -TimeLoop.TEnd 1e6

echo "-------------------------------------------------------------------------------------------------------"
echo "- Simulation with K=1e-11 m^2, random inital perturbations in the whole domain with an standard deviation of 1e-12 for the gaussian distribution -"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-11_random_s1e-12 -SpatialParams.referencePermeability 1e-11 -Problem.PeriodicPertubations_xNaCl false -Problem.RandomPertubations_xNaCl true -Problem.PertubeWholeDomain true -Problem.StddevInitialSaltMolFrac 1e-12 -Grid.File grids/grid_w60_h20_nonPeriodic.dgf -TimeLoop.TEnd 1e6

echo "------------- All simulations performed. --------------------------------------------------------------"
