#Run all simulations for the investigation of different permeabilities used in the paper

cd build-cmake/appl/
echo "-- Compile executable..."
make instability_1pncmin_tpfa

echo "-------------------------------------------------------------------------------------------------------"
echo "---------** Run simulations with different permeabilities and random initial perturbations **----------"
echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-10 m^2, random inital condition --------------------------------------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-10_random -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl false -Problem.RandomPertubations_xNaCl true -Grid.File grids/grid_w60_h20_nonPeriodic.dgf -TimeLoop.TEnd 2e6

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-11 m^2, random inital condition --------------------------------------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-11_random -SpatialParams.referencePermeability 1e-11 -Problem.PeriodicPertubations_xNaCl false -Problem.RandomPertubations_xNaCl true -Grid.File grids/grid_w60_h20_nonPeriodic.dgf -TimeLoop.TEnd 2e6

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-12 m^2, random inital condition --------------------------------------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-12_random -SpatialParams.referencePermeability 1e-12 -Problem.PeriodicPertubations_xNaCl false -Problem.RandomPertubations_xNaCl true -Grid.File grids/grid_w60_h20_nonPeriodic.dgf -TimeLoop.TEnd 1e7

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-13 m^2, random inital condition --------------------------------------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-13_random -SpatialParams.referencePermeability 1e-13 -Problem.PeriodicPertubations_xNaCl false -Problem.RandomPertubations_xNaCl true -Grid.File grids/grid_w60_h20_nonPeriodic.dgf -TimeLoop.TEnd 1.5e7

echo "-------------------------------------------------------------------------------------------------------"
echo "---------** Run simulations with different permeabilities and periodic initial perturbations **--------"
echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-10 m^2, periodic inital condition with wavelength lamba = 0.01 m -----"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-10_periodic_l001 -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.01 -TimeLoop.TEnd 2e6

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-10 m^2, periodic inital condition with wavelength lamba = 0.015 ------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-10_periodic_l0015 -SpatialParams.referencePermeability 1e-10 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.015 -TimeLoop.TEnd 5e4

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-11 m^2, periodic inital condition with wavelength lamba = 0.03 -------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-11_periodic_l003 -SpatialParams.referencePermeability 1e-11 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.03 -TimeLoop.TEnd 2e6

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-11 m^2, periodic inital condition with wavelength lamba = 0.04 -------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-11_periodic_l004 -SpatialParams.referencePermeability 1e-11 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.04 -TimeLoop.TEnd 5e5

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-12 m^2, periodic inital condition with wavelength lamba = 0.06 -------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-12_periodic_l006 -SpatialParams.referencePermeability 1e-12 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.06 -TimeLoop.TEnd 1e7

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-12 m^2, periodic inital condition with wavelength lamba = 0.12 -------"
./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-12_periodic_l012 -SpatialParams.referencePermeability 1e-12 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.12 -TimeLoop.TEnd 5e6

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-13 m^2, periodic inital condition with wavelength lamba = 0.15 -------"

./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-13_periodic_l015 -SpatialParams.referencePermeability 1e-13 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.15 -TimeLoop.TEnd 1.5e7

echo "-------------------------------------------------------------------------------------------------------"
echo "----------- Simulation with K=1e-13 m^2, periodic inital condition with wavelength lamba = 0.3 --------"

./instability_1pncmin_tpfa params.input -Problem.Name instability_K1e-13_periodic_l03 -SpatialParams.referencePermeability 1e-13 -Problem.PeriodicPertubations_xNaCl true -Problem.RandomPertubations_xNaCl false -Grid.File grids/grid_w60_h20.dgf -Problem.InitialWaveLength 0.3 -TimeLoop.TEnd 1.5e7
echo "----------- All simulations performed. ----------------------------------------------------------------"
