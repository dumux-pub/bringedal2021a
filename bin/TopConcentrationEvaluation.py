#!/usr/bin/python
import csv
import numpy as np
import datetime
import math
from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.pyplot import text

#Input parameters
#in and output parameters
numberOfFiles = 25              #total number of files which should be evaluated (including initial file)
iname = 'instability_1pncmin'   #name of the input files
oname = 'instability'           #name of output files
fileOffset = 0                  #greater than zero when we want to start the evaluation from a special file, 0: start with initial file
eps2 = 1e-7                     #needed to find the right number of zeros in the filename

#simulation parameters
resolution = 600                #number of cells in x-direction
timescale = 600                 #timestep between the output files
width = 0.6                     #width of the domain
periodic = 1                    #0: no periodic boundaries, maxima on left and right boundary are taken into account,
                                #1: periodic boundaries, just maximum on the left is taken into account
#evaluation parameters
eps = 1e-5                      #threshold for standard deviation to define t_init
useMassFraction = 0             #0: mole fractions are evaluated, 1: mass fractions are calculated and evaluted

#plot parameters
plot = 1                        #0: no plots, 1: plots are generated
plotMaximaDistance = 0          #0: no maxima distance is plotted, 1: maxima distance is plotted
fsize = 20                      #font size in plots



y_mean_max = 0.11
massOrMole = 'mol'

if useMassFraction:
    Mw = 18.015e-3 # molecular weight of water [kg/mol]
    Ms = 58.44e-3 # molecular weight of NaCl [kg/mol]
    massOrMole = 'mass'
    y_mean_max = 0.26

#Script
colnames = ["p","rho","xNaClliq","vtkValidPointMask","arc_length","Points:0","Points:1","Points:2"]

#initialize matices
#x_data wich will save salt mole fractions of every time step
x_data = np.zeros((numberOfFiles, resolution+2))
x_data = np.asarray(x_data)
maxima = np.zeros((numberOfFiles, resolution+2))
maxima = np.asarray(maxima)

#go through all the files and fill the x data in x_data
for timeStep in range(0, numberOfFiles): #goes from 0 to numberOfFiles-1
    print  "Read in and process file " + str(timeStep+fileOffset)

    #read in the files
    nulls = "0" * min(4, 5-int(math.log(timeStep+fileOffset+eps2, 10) + 1))
    name = 'PlotOverLine_Analysis/' + iname + '-' + nulls + str(timeStep+fileOffset) + '.csv'
    data = np.genfromtxt(name, delimiter=",", names=colnames)
    data = np.asarray(data)

    #save x data of every timestep in separate row of x_data, replace name with timestep
    x_data[timeStep] = data['xNaClliq']
    x_data[timeStep][0] = timeStep

    if useMassFraction:
        for i in range(2, resolution+2):
            x = x_data[timeStep][i]
            x_data[timeStep][i] = (-x *Ms)/(-Mw-x*(Ms-Mw))

    #determine local maxima
    #structure of maxima: index 0 - no meaning, index 1 -additional value at border not used, index 2 - node at boundary, index 3 to resolution - inside nodes, index resolution+1 - node at boundary
    for i in range(3, resolution+1): #leave out the nodes at the borders, goes from 3 to resolution
        if x_data[timeStep][i] > x_data[timeStep][i-1] and x_data[timeStep][i] >= x_data[timeStep][i+1]: #defines local maximas, if two points build one local maximum just the left point is defined as maximum
            maxima[timeStep][i] = 1
        else:
            maxima[timeStep][i] = 0
    # look at nodes at boundaries
    if x_data[timeStep][2] >= x_data[timeStep][3]:
        maxima[timeStep][2] = 1
    if x_data[timeStep][resolution+1] > x_data[timeStep][resolution] and not periodic:
        maxima[timeStep][resolution+1] = 1

#calculate mean value and stddev of x data and maxima for each timestep
stddev = np.std(x_data[:, 2:resolution+2], 1) #np.std(matrix/datafield, direction), leave out the timestep at 0 and first value as it appears double using tpfa, calculate standard deviations for every row/timestep
mean = np.mean(x_data[:, 2:resolution+2], 1)

stddev_max = np.std(maxima[:, 2:resolution+2], 1) #leave out the timestep at 0
mean_max = np.mean(maxima[:, 2:resolution+2], 1)

cellSize = width/resolution
mean_maximaDistance = np.zeros((len(mean_max)))
t_init = 0
x=0.0

#calculate mean distance between fingers
for i in range(len(mean_max)):
    if mean_max[i] == 0.0:
        mean_maximaDistance[i] = 0
    else:
        mean_maximaDistance[i] = cellSize / mean_max[i] #calculates cell width * cells per Finger

# figure out first time when stddev exceeds eps
for i in range(len(stddev)):
    if stddev[i] > eps and x == 0.0:
        t_init = i
        x=1.0
    elif stddev[i] <= eps:
        x=0.0
    else:
        x=1.0

time = range(fileOffset*timescale, (len(stddev)+fileOffset)*timescale, timescale)
analysis = zip(time, mean, stddev, mean_max, mean_maximaDistance)

with open('Evaluation/' + oname + '_' + massOrMole + '_analysis.csv', mode='w') as t_file:
    t_writer = csv.writer(t_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    t_writer.writerow(('time [s]', 'mean ' + massOrMole + '_NaCl','standard deviation ' + massOrMole + '_NaCl','number of maxima/fingers per cell','distance between maxima/fingers'))
    for row in analysis:
        t_writer.writerow(row)
t_file.close()

t_xNaCl_mean = np.where(mean == np.amax(mean, 0)) #timestep with maximal mean value of x
t_xNaCl_stddev = np.where(stddev == np.amax(stddev, 0)) #timestep with maximal standarddev of x
t_InstPerCell_max = np.where(mean_max == np.amax(mean_max, 0)) #timestep with maximal number of instabilities per cell/ minimal distance between fingers
t_xNaCl_stddev_min = np.where(stddev == np.amin(stddev, 0)) #timestep with minimal standarddeviation

with open('Evaluation/' + oname + '_' + massOrMole + '_parameters.csv', mode='w') as t_file:
    t_writer = csv.writer(t_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    t_writer.writerow((oname, oname))
    t_writer.writerow(('--- t_init ---', 'time for which stddev is greater than' + str(eps)))
    t_writer.writerow(('Timestep onset instability t_i: ', t_init))
    t_writer.writerow(('Calculated time onset instability t_i: ', t_init*timescale))
    t_writer.writerow(('Standard deviation xNaCl at t_i: ', stddev[t_init]))
    t_writer.writerow(('Mean xNaCl at t_i: ', mean[t_init]))
    t_writer.writerow(('Distance between instabilities at t_i: ', mean_maximaDistance[t_init]))
    t_writer.writerow(('Instabilities per cell at t_i: ', mean_max[t_init]))
    t_writer.writerow(('--- t_mean ---', 'time for which mean value is maximal used as start for phase 3'))
    t_writer.writerow(('Timestep with maximal xNaCl mean t_mean: ', t_xNaCl_mean[0][0]))
    t_writer.writerow(('Calculated time with maximal xNaCl mean t_mean: ', t_xNaCl_mean[0][0]*timescale))
    t_writer.writerow(('maximal xNaCl mean: ', mean[t_xNaCl_mean[0][0]]))
    t_writer.writerow(('Distance between instabilities at t_mean: ', mean_maximaDistance[t_xNaCl_mean[0][0]]))
    t_writer.writerow(('Instabilities per cell at t_mean: ', mean_max[t_xNaCl_mean[0][0]]))
    t_writer.writerow(('--- t_stddev ---', 'time for which stddev is maximal'))
    t_writer.writerow(('Timestep with maximal xNaCl standard deviation t_stddev: ', t_xNaCl_stddev[0][0]))
    t_writer.writerow(('Calculated time with maximal xNaCl standard deviation t_stddev: ', t_xNaCl_stddev[0][0]*timescale))
    t_writer.writerow(('maximal xNaCl standartd deviation: ', stddev[t_xNaCl_stddev[0][0]]))
    t_writer.writerow(('Distance between instabilities at t_stddev: ', mean_maximaDistance[t_xNaCl_stddev[0][0]]))
    t_writer.writerow(('Instabilities per cell at t_stddev: ', mean_max[t_xNaCl_stddev[0][0]]))
    t_writer.writerow(('--- t_cell ---', 'time for which minimal distance between fingers occure'))
    t_writer.writerow(('Timestep of maximal instabilities per cell: ', t_InstPerCell_max[0][0]))
    t_writer.writerow(('Calculated time of maximal instabilities per cell: ', t_InstPerCell_max[0][0]*timescale))
    t_writer.writerow(('distance between instabilities at t_cell: ', mean_maximaDistance[t_InstPerCell_max[0][0]]))
    t_writer.writerow(('--- onset time t_xNaCl_stddev_min ---', 'time for which stddev is minimal used at onset time'))
    t_writer.writerow(('timestep with minimal standarddeviation: ', t_xNaCl_stddev_min[0][len(t_xNaCl_stddev_min[0])-1])) #using last occurence of minimum
    t_writer.writerow(('Calculated time with minimal standarddeviation: ', t_xNaCl_stddev_min[0][len(t_xNaCl_stddev_min[0])-1]*timescale)) #using last occurence of minimum
    t_writer.writerow(('minimal standarddeviation: ', stddev[t_xNaCl_stddev_min[0][len(t_xNaCl_stddev_min[0])-1]]))
    t_writer.writerow(('Mean at t_xNaCl_stddev_min: ', mean[t_xNaCl_stddev_min[0][len(t_xNaCl_stddev_min[0])-1]]))
    t_writer.writerow(('Distance between instabilities at t_xNaCl_stddev_min: ', mean_maximaDistance[t_xNaCl_stddev_min[0][len(t_xNaCl_stddev_min[0])-1]]))
    t_writer.writerow(('Instabilities per cell at t_xNaCl_stddev_min: ', mean_max[t_xNaCl_stddev_min[0][len(t_xNaCl_stddev_min[0])-1]]))
t_file.close()

if plot:
    fig = plt.figure(figsize=(10.3, 6.0))  # create a figure object
    plt.rc('font', size=fsize)
    ax = fig.add_subplot(1, 1, 1)
    ax1 = ax.twinx()
    plt.subplots_adjust(left=0.13, bottom=0.13, right=0.87, top=0.95, wspace=0, hspace=0.0)


    #for i in range(len(stddev)):
    plot1=ax1.plot(range(fileOffset*timescale, (len(stddev)+fileOffset)*timescale, timescale), stddev, color='red', markersize = 2.0, linestyle='', marker='o', label='standard deviation')
    plot2=ax.plot(range(fileOffset*timescale, (len(mean)+fileOffset)*timescale, timescale), mean, color='blue', markersize = 2.0, linestyle='', marker='o', label='mean')
    plot4=plt.axvline(x=(t_xNaCl_mean[0]+fileOffset)*timescale, marker='', color='blue', label='maximal mean')
    plot7=plt.axvline(x=(t_xNaCl_stddev_min[0][len(t_xNaCl_stddev_min[0])-1]+fileOffset)*timescale, marker='', color='violet', label='onset')

    if plotMaximaDistance:
        massOrMole = massOrMole + '_fingerDistance'
        plot=ax.plot(range(fileOffset*timescale, (len(mean_maximaDistance)+fileOffset)*timescale, timescale), mean_maximaDistance, color='green', markersize = 2.0, linestyle='', marker='o', label='distance of fingers')
        plot6=plt.axvline(x=(t_InstPerCell_max[0][0]+fileOffset)*timescale, marker='', color='green', label='minimal distance')

    from matplotlib import ticker
    formatter = ticker.ScalarFormatter(useMathText=True)
    formatter.set_scientific(True)
    formatter.set_powerlimits((-2,2))

    xaxislabel = "time [s]"
    ax.xaxis.set_major_formatter(formatter)

    ax.set_xlim((0+fileOffset)*timescale, (len(stddev)+fileOffset)*timescale)
    ax.set_xlabel(xaxislabel, fontsize = fsize)
    ax.tick_params(axis='both', which='major', labelsize=fsize)
    ax.set_ylim(0.0, y_mean_max)
    if useMassFraction:
        ax.set_ylabel('mean value of $X^{NaCl}$', fontsize = fsize)
        ax1.set_ylabel('standard deviation of $X^{NaCl}$', fontsize = fsize)
    else:
        ax.set_ylabel('mean value of $x^{NaCl}$', fontsize = fsize)
        ax1.set_ylabel('standard deviation of $x^{NaCl}$', fontsize = fsize)
    ax1.tick_params(axis='both', which='major', labelsize=fsize)
    ax1.set_ylim(1e-10, 1)
    ax1.set_yscale('log')

    lines, labels = ax.get_legend_handles_labels()
    lines2, labels2 = ax1.get_legend_handles_labels()
    ax1.legend(lines + lines2, labels + labels2, loc='lower right', markerscale = 2.0, fontsize = fsize)

    plt.savefig('Evaluation/' + oname + '_' + massOrMole + '_plot.png', format = 'png', dpi = 200)
    plt.savefig('Evaluation/' + oname + '_' + massOrMole + '_plot.pdf', format = 'pdf')
