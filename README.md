Summary
============
This repository contains the source code to reproduce the numerical simulations presented in the paper:

Carina Bringedal, Theresa Schollenberger, G. J. M. Pieters, C. J. van Duijn and Rainer Helmig. Evaporation-driven density instabilities in saturated porous media. Transport in Porous Media. 2022. doi:10.1007/s11242-022-01772-w.

The results of the numerical simulations used in the paper and produced by the code presented in this git-repository are available in the following Dataset on [DaRUS](https://darus.uni-stuttgart.de/):
Schollenberger, Theresa, 2022, "Replication Data for the numerical simulations in: Evaporation-driven density instabilities in saturated porous media", [https://doi.org/10.18419/darus-2578](https://doi.org/10.18419/darus-2578), DaRUS.

Installation
============
The easiest way of installation is to use the script [installbringedal2021a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a/-/blob/main/installbringedal2021a.sh) provided in this repository.
Using `wget`, you can simply install all dependent modules by typing:

```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a/-/blob/main/installbringedal2021a.sh
chmod u+x installbringedal2021a.sh
./installbringedal2021a.sh
```
This will create a sub-folder `DUMUX`, clone all modules into it, configure the entire project and build the applications contained in this module. Note that there is also a docker image available which can be used as described in [docker/README.md](https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a/-/blob/main/docker/README.md).

Compile and run the simulation
============
To compile the simulation use the following commands:
```bash
cd bringedal2021a/build-cmake/appl
make instability_1pncmin_tpfa
```
To then run the simulation use the following command:
```bash
./instability_1pncmin_tpfa
```
If you want to rerun all simulations performed for the paper you can use the following bash scripts in the `bin` folder:
* [runSimulation_permeabilities.sh](https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a/-/blob/main/bin/runSimulations_permeabilities.sh): runs all numerical simulations presented in section 5 of the paper with the different permeabilities for random and periodic initial perturbations
* [runSimulation_initial.sh](https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a/-/blob/main/bin/runSimulations_initial.sh): runs all numerical simulations presented in section 4.5 of the paper investigating different initial perturbations
* [runSimulation_convergence.sh](https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a/-/blob/main/bin/runSimulations_convergence.sh): runs all numerical simulations presented in appendix E of the paper used for the grid and time-step convergence study

To use the scripts use the following commands:
```bash
cd bringedal2021a/
chmod u+x bin/runSimulation_insertName.sh
./bin/runSimulation_insertName.sh
```

Evaluation
============
To evaluate the results of the simulation and generate a plot corresponding to the ones in figure 12 in the paper the following steps are necessary:
We extract all values in the top grid cells of every vtu file by the [extractlinedata.py](https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a/-/blob/main/bin/extractlinedata.py) script and store them in a new folder `PlotOverLine_Analysis`:

```bash
cd bringedal2021a/
mkdir PlotOverLine_Analysis/
pvpython bin/extractlinedata.py -f build-cmake/appl/instability_1pncmin-* -o PlotOverLine_Analysis/ -p1 0 0.1999 0 -p2 0.6 0.1999 0 -r 600 -v 1
```
Note that the option -f has to be adjusted to the name of the vtu-files, when using the bash scripts listed above to run the simulations. This data is needed to run the evaluation script which calculates the mean value and standard deviation of salt mole fraction at the top and plot them over time:
```bash
mkdir Evaluation
pvpython bin/TopConcentrationEvaluation.py
```
Please note that you can adapt certain parameters in this script which are listed at the top of [TopConcentrationEvaluation.py](https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a/-/blob/main/bin/TopConcentrationEvaluation.py). For example the number of output files you want to evaluate. Please adapt this parameter *numberOfFiles* to the number of available output files from the simulation. Also the name of input files *iname* has to be adjusted to the name of the vtu-files, when using the bash scripts listed above to run the simulations. Further the parameter *periodic* has to be set to 1 if you use periodic boundary conditions at the sides and to 0 if not. The script will produce four output files in the folder `Evaluation`:
* analysis.csv: contains the mean value and standard deviation of the salt mole fraction over time as well as number of fingers per grid cell and distance between fingers
* parameters.csv: contains a list of specific key parameters like the onset time of this simulation
* plot.pdf and plot.png: are plots saved in pdf and png format which plot the mean value and standard deviation of the salt mole fraction over time as well as the onset time and maximal mean value
