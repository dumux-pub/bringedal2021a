// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup OnePNCMinTests
 * \brief Problem where water is evaporating at the top boundary.
 */
#ifndef DUMUX_STABILITY_1P_STEP4_PROBLEM_HH
#define DUMUX_STABILITY_1P_STEP4_PROBLEM_HH

#include <dune/alugrid/grid.hh>
#include <random>

#include <dumux/common/properties.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/custombrine.hh>

#include <dumux/material/components/nacl.hh>
#include <dumux/material/components/granite.hh>
#include <dumux/material/solidsystems/compositionalsolidphase.hh>

#include "spatialparams1p.hh"
#include <dumux/1piofields.hh>
#include <dumux/porousmediumflow/1pnc/iofields.hh>
#include <dumux/porousmediumflow/1pncmin/model.hh>
#include <dumux/porousmediumflow/mineralization/iofields.hh>

namespace Dumux {

template <class TypeTag>
class StabilityOnePStep4Problem;

namespace Properties {
// Create new type tags
namespace TTag {
#if NONISOTHERMAL
    struct StabilityStep4 { using InheritsFrom = std::tuple<OnePMinNI>; };
#else
    struct StabilityStep4 { using InheritsFrom = std::tuple<OnePNCMin>; };
#endif
struct StabilityStep4Box { using InheritsFrom = std::tuple<StabilityStep4, BoxModel>; };
struct StabilityStep4CCTpfa { using InheritsFrom = std::tuple<StabilityStep4, CCTpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StabilityStep4> { using type = Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::StabilityStep4> { using type = StabilityOnePStep4Problem<TypeTag>; };

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::StabilityStep4>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::CustomBrine<Scalar, Components::H2O<Scalar>>;
};

//! Set the reduced vtk output fields otherwise the detailed output from mineralization and 2pnc is used
#if REDUCEDOUTPUT
    template<class TypeTag>
    struct IOFields<TypeTag, TTag::StabilityStep4> { using type = MineralizationIOFields<CustomOnePNCIOFields>; };
#endif

// Set the solid system
template<class TypeTag>
struct SolidSystem<TypeTag, TTag::StabilityStep4>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using ComponentOne = Components::NaCl<Scalar>;
    using ComponentTwo = Components::Granite<Scalar>;
    static constexpr int numInertComponents = 1;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, ComponentOne, ComponentTwo, numInertComponents>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::StabilityStep4>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = Salinization1pSpatialParams<GridGeometry, Scalar>;
};

} // end namespace Properties



template <class TypeTag>
class StabilityOnePStep4Problem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using Vertex = typename GridView::template Codim<GridView::dimensionworld>::Entity;

    enum
    {
        // primary variable indices
        pressureIdx = Indices::pressureIdx,

        // component indices
        xwNaClIdx = FluidSystem::NaClIdx,
        precipNaClIdx = FluidSystem::numComponents,

        // Indices of the components
        H2OIdx = FluidSystem::H2OIdx,
        NaClIdx = FluidSystem::NaClIdx,

        // Indices of the phases
        liquidPhaseIdx = FluidSystem::liquidPhaseIdx,

        // index of the solid phase
        sPhaseIdx = SolidSystem::comp0Idx,

        // Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx, //water component
        precipNaClEqIdx = Indices::conti0EqIdx + FluidSystem::numComponents,

        #if NONISOTHERMAL
            energyEqIdx = Indices::energyEqIdx,
        #endif

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GlobalPosition = typename SubControlVolume::GlobalPosition;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;

public:
    StabilityOnePStep4Problem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        //Fluidsystem
        nTemperature_           = getParam<int>("FluidSystem.NTemperature");
        nPressure_              = getParam<int>("FluidSystem.NPressure");
        pressureLow_            = getParam<Scalar>("FluidSystem.PressureLow");
        pressureHigh_           = getParam<Scalar>("FluidSystem.PressureHigh");
        temperatureLow_         = getParam<Scalar>("FluidSystem.TemperatureLow");
        temperatureHigh_        = getParam<Scalar>("FluidSystem.TemperatureHigh");
        name_                   = getParam<std::string>("Problem.Name");

        //bottom Dirichlet boundary
        bottomPressure_         = getParam<Scalar>("Problem.BottomPressure");
        bottom_X_NaCl_          = getParam<Scalar>("Problem.BottomSaltMassFrac");

        #if NONISOTHERMAL
            bottomTemperature_  = getParam<Scalar>("Problem.BottomTemperature");
        #endif

        //top evaporation boundary condition
        evaporationRate_                = getParam<Scalar>("FreeFlow.EvaporationRate");
        useDensityCorrectedEvaporation_ = getParam<bool>("Problem.UseDensityCorrectedEvaporation");

        //problem
        name_                   = getParam<std::string>("Problem.Name");
        temperature_            = getParam<Scalar>("Problem.Temperature");

        //inital conditions
        initPressure_                   = getParam<Scalar>("Problem.InitialPressure");
        init_X_NaCl_                    = getParam<Scalar>("Problem.InitialSaltMassFrac");
        useRandomPertubations_x_NaCl_   = getParam<bool>("Problem.RandomPertubations_xNaCl");
        usePeriodicPertubations_x_NaCl_ = getParam<bool>("Problem.PeriodicPertubations_xNaCl");
        pertubeWholeDomain_             = getParam<bool>("Problem.PertubeWholeDomain");
        waveLength_                     = getParam<Scalar>("Problem.InitialWaveLength");

        unsigned int codim = GetPropType<TypeTag, Properties::GridGeometry>::discMethod == DiscretizationMethod::box ? dim : 0;
        permeability_.resize(gridGeometry->gridView().size(codim));
        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);

        if (usePeriodicPertubations_x_NaCl_){
            amplitude_ = getParam<Scalar>("Problem.Amplitude");
        }
        else if (useRandomPertubations_x_NaCl_){
            //generate random initial mole fraction distribution
            stddev_init_x_NaCl_  = getParam<Scalar>("Problem.StddevInitialSaltMolFrac");

            unsigned seed = 1; //std::chrono::system_clock::now().time_since_epoch().count();
            std::default_random_engine generator (seed);
            std::normal_distribution<double> distribution (massToMoleFrac_(init_X_NaCl_), stddev_init_x_NaCl_);

            auto numElems = this->gridGeometry().elementMapper().size();
            x_.resize(numElems, 0.0);

            for (const auto& element : elements(this->gridGeometry().gridView()))
            {
                int eIdx = this->gridGeometry().elementMapper().index(element);
                x_[eIdx] = distribution(generator);
            }

            auto numVertex = this->gridGeometry().vertexMapper().size();
            xBox_.resize(numVertex, 0.0);

            for (const auto& vertex : vertices(this->gridGeometry().gridView()))
            {
                int vIdx = this->gridGeometry().vertexMapper().index(vertex);
                xBox_[vIdx] = distribution(generator);
            }
        }

        //calculation of corresponding initial density
        FluidState fluidState;
        fluidState.setPressure(liquidPhaseIdx, initPressure_);
        fluidState.setTemperature(temperature());
        fluidState.setSaturation(liquidPhaseIdx, 1.0);
        fluidState.setMassFraction(liquidPhaseIdx, NaClIdx, init_X_NaCl_);
        initialDensity_ = FluidSystem::density(fluidState, liquidPhaseIdx);
    }

    /*!
     * \brief The current time.
     */
    void setTime( Scalar time )
    {
        time_ = time;
    }

    /*!
     * \brief The time step size.
     *
     * This is used to calculate the source term.
     */
    void setTimeStepSize( Scalar timeStepSize )
     {
        timeStepSize_ = timeStepSize;
     }


    /*!
     * \name Problem parameters
     */

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return temperature_; }


    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        // default to Neumann
        bcTypes.setAllNeumann();

        // Dirichlet bc at the column bottom
        if(globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_)
        {
            bcTypes.setAllDirichlet();
        }

        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);

        const auto g = this->spatialParams().gravity(globalPos)[dimWorld-1];
        Scalar h = this->gridGeometry().bBoxMax()[1];

        priVars[pressureIdx]   = initPressure_ - (h*initialDensity_*g); // Bottom boundary pressure
        priVars[xwNaClIdx]     = massToMoleFrac_(bottom_X_NaCl_);// mole fraction salt
        priVars[precipNaClIdx] = 0.0;// precipitated salt

        #if NONISOTHERMAL
            priVars[energyEqIdx] = bottomTemperature_;// temperature
        #endif

        return priVars;
    }


    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub control volume face
     *
     * Negative values mean influx.
     * E.g. for the mass balance that would be the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {

        PrimaryVariables values(0.0);

        //evaporation starts after a certain time -> episode length
        static const Scalar episodeLength = getParam<Scalar>("TimeLoop.EpisodeLength");
        if(time_ > episodeLength){
            const auto& globalPos = scvf.ipGlobal();
            const Scalar hmax = this->gridGeometry().bBoxMax()[1];

            if (globalPos[1] > hmax - eps_)
            {
                auto evaporationRate = 0.0;
                if(useDensityCorrectedEvaporation_){
                    const auto& volVars = elemVolVars[scvf.insideScvIdx()];
                    FluidState fluidState = volVars.fluidState();
                    const auto molarDensity = FluidSystem::molarDensity(fluidState, liquidPhaseIdx);
                    evaporationRate = evaporationRate_ * molarDensity;
                }
                else{
                    evaporationRate = evaporationRate_;
                }

                //liquid phase constant evaporation rate
                values[conti0EqIdx] = evaporationRate;
            }
        }
        return values;
    }

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The global position
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Element& element) const
    {
        PrimaryVariables priVars(0.0);
        const auto globalPos = element.geometry().center();
        Scalar h = this->gridGeometry().bBoxMax()[1];

        priVars[xwNaClIdx]   = massToMoleFrac_(init_X_NaCl_);

        //random initial salt concentration
        //perturbation of the whole domain
        if(pertubeWholeDomain_){
            if (useRandomPertubations_x_NaCl_){
                int eIdx = this->gridGeometry().elementMapper().index(element);
                priVars[xwNaClIdx] = x_[eIdx];
            }
            else if (usePeriodicPertubations_x_NaCl_){
                priVars[xwNaClIdx] = massToMoleFrac_(init_X_NaCl_ + amplitude_ * cos(2*M_PI / waveLength_ * globalPos[0]));
            }
            else{
                //constant initial salt concentration
                priVars[xwNaClIdx]   = massToMoleFrac_(init_X_NaCl_);
            }
        }
        //perturbation just at the top
        else{
            if (useRandomPertubations_x_NaCl_&& globalPos[dimWorld-1] > (h - 3e-4)){
                int eIdx = this->gridGeometry().elementMapper().index(element);
                priVars[xwNaClIdx] = x_[eIdx];
            }
            else if (usePeriodicPertubations_x_NaCl_ && globalPos[dimWorld-1] > (h - 3e-4)){
                priVars[xwNaClIdx] = massToMoleFrac_(init_X_NaCl_ + amplitude_ * cos(2*M_PI / waveLength_ * globalPos[0]));
            }
            else{
                //constant initial salt concentration
                priVars[xwNaClIdx]   = massToMoleFrac_(init_X_NaCl_);
            }
        }

        const auto g = this->spatialParams().gravity(globalPos)[dimWorld-1];  // =-9.81
        priVars[pressureIdx] = initPressure_ + initialDensity_*g*(globalPos[dimWorld-1]-h);
        priVars[precipNaClIdx] = 0.0; // [kg/m^3]

        #if NONISOTHERMAL
            priVars[energyEqIdx] = temperature_; // [K]
        #endif

        return priVars;
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-controlvolume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The subcontrolvolume
     *
     * Positive values mean that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);

        const auto& volVars = elemVolVars[scv];

        Scalar moleFracNaCl_wPhase = volVars.moleFraction(liquidPhaseIdx, NaClIdx);
        Scalar massFracNaCl_Max_wPhase = this->spatialParams().solubilityLimit();
        Scalar moleFracNaCl_Max_wPhase = massToMoleFrac_(massFracNaCl_Max_wPhase); // = 0.0977248 mol NaCl/ mol Solution
        Scalar saltPorosity = this->spatialParams().minimalPorosity(element, scv);

        // precipitation of amount of salt whic hexeeds the solubility limit
        using std::abs;
        Scalar precipSalt = volVars.porosity() * volVars.molarDensity(liquidPhaseIdx)
                                               * volVars.saturation(liquidPhaseIdx)
                                               * abs(moleFracNaCl_wPhase - moleFracNaCl_Max_wPhase)
                                               / timeStepSize_;
        if (moleFracNaCl_wPhase < moleFracNaCl_Max_wPhase)
            precipSalt *= -1;

        // make sure we don't dissolve more salt than previously precipitated
        if (precipSalt*timeStepSize_ + volVars.solidVolumeFraction(sPhaseIdx)* volVars.solidComponentMolarDensity(sPhaseIdx)< 0)
            precipSalt = -volVars.solidVolumeFraction(sPhaseIdx)* volVars.solidComponentMolarDensity(sPhaseIdx)/timeStepSize_;

        // make sure there is still pore space available for precipitation
        if (volVars.solidVolumeFraction(sPhaseIdx) >= this->spatialParams().referencePorosity(element, scv) - saltPorosity  && precipSalt > 0)
            precipSalt = 0;

        source[conti0EqIdx + NaClIdx] += -precipSalt;
        source[precipNaClEqIdx] += precipSalt;
        return source;
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter.
     *
     * Function is called by the output module on every write.
     */

    const std::vector<Scalar>& getPermeability()
    {
        return permeability_;
    }

    void updateVtkOutput(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->gridGeometry());

            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                permeability_[dofIdxGlobal] = volVars.permeability();
            }
        }
    }

    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    {
        return 1.0;
    }

private:

    /*!
     * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction.
     *
     * \param XwNaCl The XwNaCl [kg NaCl / kg solution]
     */
    static Scalar massToMoleFrac_(Scalar XwNaCl)
    {
       const Scalar Mw = 18.015e-3; //FluidSystem::molarMass(H2OIdx); /* molecular weight of water [kg/mol] */
       const Scalar Ms = 58.44e-3;  //FluidSystem::molarMass(NaClIdx); /* molecular weight of NaCl  [kg/mol] */

       const Scalar X_NaCl = XwNaCl;
       /* XwNaCl: conversion from mass fraction to mol fraction */
       auto xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
       return xwNaCl;
    }


    std::string name_;

    Scalar evaporationRate_;
    Scalar initPressure_;
    Scalar initTopGasSaturation_;
    Scalar init_X_NaCl_;
    Scalar temperature_;
    Scalar stddev_init_x_NaCl_;
    Scalar waveLength_;
    Scalar amplitude_;
    bool useRandomPertubations_x_NaCl_;
    bool usePeriodicPertubations_x_NaCl_;
    bool pertubeWholeDomain_;
    bool useDensityCorrectedEvaporation_;

    Scalar bottomPressure_;
    Scalar bottomGasSaturation_;
    Scalar bottom_X_NaCl_;
    Scalar initialDensity_;

    #if NONISOTHERMAL
        Scalar bottomTemperature_;
    #endif

    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
    int nTemperature_;
    int nPressure_;

    Scalar time_ = 0.0;
    Scalar timeStepSize_ = 0.0;
    static constexpr Scalar eps_ = 1e-6;

    std::vector<Scalar> permeability_;

    Dumux::GnuplotInterface<Scalar> gnuplot_;
    Dumux::GnuplotInterface<Scalar> gnuplot2_;
    std::vector<Scalar> x_;
    std::vector<Scalar> xBox_;
    std::vector<Scalar> y_;
    std::vector<Scalar> y2_;
};

} // end namespace Dumux

#endif
